/*
 *       File name:  aghermann/ica/forward-decls.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-07
 *
 *         Purpose:  
 *
 *         License:  GPL
 */

#ifndef AGH_ICA_FORWARD_DECLS_H_
#define AGH_ICA_FORWARD_DECLS_H_

namespace ica {

class CFastICA;

}

#endif
