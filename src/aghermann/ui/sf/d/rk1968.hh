/*
 *       File name:  aghermann/ui/sf/d/rk1968.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-08-04
 *
 *         Purpose:  scoring facility RK1968 assistant
 *
 *         License:  GPL
 */

#ifndef AGH_AGHERMANN_UI_SF_D_RK1968_H_
#define AGH_AGHERMANN_UI_SF_D_RK1968_H_

#include "libsigfile/page.hh"
#include "aghermann/rk1968/rk1968.hh"
#include "aghermann/ui/ui++.hh"
#include "aghermann/ui/dirlevel-storable-adapter.hh"
#include "aghermann/ui/sf/sf.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace agh {
namespace ui {

struct SScoringFacility::SRK1968Dialog
  : public SDirlevelStorableAdapter<agh::rk1968::CScoreAssistant> {

        DELETE_DEFAULT_METHODS (SRK1968Dialog);

      // ctor, dtor
        explicit SRK1968Dialog (SScoringFacility&);
       ~SRK1968Dialog ();

        vector<sigfile::SPage>
                backup;

        bool    suppress_preview_handler;

      // parent
        SScoringFacility&
                _p;

      // widgets
        GtkBuilder
                *builder;

        GtkDialog
                *wSFRK;

        GtkListStore
                *mSFRKProfiles;
        GtkButton
                *bSFRKProfileSave,
                *bSFRKProfileRevert,
                *bSFRKProfileDiscard;
        GtkComboBox
                *eSFRKProfileList;
        gulong  eSFRKProfileList_changed_cb_handler_id;

        GtkTextView
                *eSFRKScript;
        GtkTextBuffer
                *tSFRKScript;

        GtkToggleButton
                *bSFRKPreview;
        GtkButton
                *bSFRKDismiss,
                *bSFRKApply;

        GtkLabel
                *lSFRKTopInfo,
                *lSFRKBottomInfo;
        GtkDialog
                *wSFRKProfileSave;
        GtkEntry
                *eSFRKProfileSaveName;
        GtkToggleButton
                *eSFRKProfileSaveOriginSubject,
                *eSFRKProfileSaveOriginExperiment,
                *eSFRKProfileSaveOriginUser;
        GtkButton
                *bSFRKProfileSaveOK;
};


}} // namespace aghui

extern "C" {

void eSFRKProfileList_changed_cb( GtkComboBox*, gpointer);
void bSFRKProfileSave_clicked_cb( GtkButton*, gpointer);
void bSFRKProfileDiscard_clicked_cb( GtkButton*, gpointer);
void bSFRKProfileRevert_clicked_cb( GtkButton*, gpointer);

void wSFRK_show_cb( GtkWidget*, gpointer);
void wSFRK_hide_cb( GtkWidget*, gpointer);

void eSFRK_any_profile_value_changed_cb( GtkSpinButton*, gpointer);

void bSFRKPreview_toggled_cb( GtkToggleButton*, gpointer);
void bSFRKApply_clicked_cb( GtkButton*, gpointer);
void bSFRKDismiss_clicked_cb( GtkButton*, gpointer);

void eSFRK_any_profile_origin_toggled_cb(GtkRadioButton*, gpointer);
void eSFRKProfileSaveName_changed_cb(GtkEditable*, gpointer);

}

#endif
