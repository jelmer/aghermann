/*
 *       File name:  aghermann/ui/sf/d/artifacts_cb.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2012-10-05
 *
 *         Purpose:  scoring facility: artifact detection dialog callbacks
 *
 *         License:  GPL
 */

#include "aghermann/ui/misc.hh"
#include "aghermann/ui/mw/mw.hh"
#include "aghermann/ui/sf/channel.hh"
#include "artifacts.hh"

using namespace std;
using namespace agh::ui;



extern "C" {

void
wSFAD_show_cb(
        GtkWidget*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;
        auto& SF = AD._p;
        SF.artifacts_dialog_shown = true;

        AD.load_profiles();
        if ( AD.profiles.empty() )
                AD.profiles.emplace_back(
                        *SF._p.ED,
                        agh::SExpDirLevelId {SF._p.ED->group_of(SF.csubject()), SF.csubject().id, SF.session()});
        AD.populate_combo();
        if ( gtk_combo_box_get_active( AD.eSFADProfileList) == -1 )
                gtk_combo_box_set_active( AD.eSFADProfileList, 0);

        AD.set_profile_manage_buttons_visibility();

        for ( auto& W : {AD.cSFADMCBasedExpander, AD.cSFADFlatExpander, AD.cSFADEMGExpander} ) {
                g_signal_emit_by_name( W, "activate");
                g_signal_emit_by_name( W, "activate");
        }

        gtk_widget_set_sensitive( (GtkWidget*)AD.bSFADApply, FALSE);
        AD.suppress_preview_handler = true;
        gtk_toggle_button_set_active( AD.bSFADPreview, FALSE);
        AD.suppress_preview_handler = false;

        AD.using_channel = AD._p.using_channel;  // because the latter is mutable, and AD isn't modal

        gtk_label_set_text(
                AD.lSFADInfo,
                snprintf_buf( "Artifact detection in %s", AD.using_channel->name()));
        gtk_label_set_text(
                AD.lSFADDirtyPercent,
                snprintf_buf( "%4.2f%% marked", AD.using_channel->calculate_dirty_percent()));
}

gboolean
wSFAD_delete_event_cb(
        GtkWidget*,
        GdkEvent*,
        const gpointer userdata)
{
        bSFADDismiss_clicked_cb( NULL, userdata);
        return TRUE;
}

void
wSFAD_close_cb(
        GtkWidget*,
        const gpointer userdata)
{
        bSFADDismiss_clicked_cb( NULL, userdata);
}



void
cSFADMCBasedExpander_activate_cb(
        GtkExpander* expander,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        bool yesitis = !gtk_expander_get_expanded( expander);
        auto& P = AD.Pp2.Pp.MC;
        gtk_label_set_text(
                AD.lSFADMCBasedSummary,
                yesitis
                ? ""
                : snprintf_buf(
                        "at:%g thr:%g..%g f %g:%g:%g g:%g bp:%g E:%g d:%g..%g hs:%zu sm:%zu estE:%d range:%d",
                        P.scope, P.upper_thr, P.lower_thr, P.f0, P.fc, P.bandwidth, P.mc_gain, P.iir_backpolate,
                        P.E, P.dmin, P.dmax, P.sssu_hist_size, P.smooth_side,
                        P.estimate_E, P.use_range));
}

void
cSFADFlatExpander_activate_cb(
        GtkExpander* expander,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        bool yesitis = !gtk_expander_get_expanded( expander);
        auto& P = AD.Pp2.Pp;
        gtk_label_set_text(
                AD.lSFADFlatSummary,
                yesitis
                ? ""
                : snprintf_buf(
                        "min_size:%g pad:%g",
                        P.flat_min_size, P.flat_pad));
}

void
cSFADEMGExpander_activate_cb(
        GtkExpander* expander,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        bool yesitis = !gtk_expander_get_expanded( expander);
        auto& P = AD.Pp2.Pp;
        gtk_label_set_text(
                AD.lSFADEMGSummary,
                yesitis
                ? ""
                : snprintf_buf(
                        "factor:%g run:%g",
                        P.emg_min_steadytone_factor, P.emg_min_steadytone_run));
}



void
eSFADMCBasedConsider_toggled_cb(
        GtkToggleButton *b,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        bool enabled = gtk_toggle_button_get_active( b);
        gtk_widget_set_sensitive(
                (GtkWidget*)AD.cSFADMCBased,
                enabled);

        AD.eX_any_profile_value_changed_cb();
}

void
eSFADFlatConsider_toggled_cb(
        GtkToggleButton *b,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        bool enabled = gtk_toggle_button_get_active( b);
        gtk_widget_set_sensitive(
                (GtkWidget*)AD.cSFADFlat,
                enabled);

        AD.eX_any_profile_value_changed_cb();
}

void
eSFADEMGConsider_toggled_cb(
        GtkToggleButton *b,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        bool enabled = gtk_toggle_button_get_active( b);
        gtk_widget_set_sensitive(
                (GtkWidget*)AD.cSFADEMG,
                enabled);

        AD.eX_any_profile_value_changed_cb();
}

void
eSFAD_any_profile_value_changed_cb(
        GtkSpinButton* button,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;
        AD.eX_any_profile_value_changed_cb();
}

void
eSFADEstimateE_toggled_cb(
        GtkToggleButton *b,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        auto state = gtk_toggle_button_get_active( b);
        gtk_widget_set_visible(
                (GtkWidget*)AD.cSFADWhenEstimateEOn,
                state);
        gtk_widget_set_visible(
                (GtkWidget*)AD.cSFADWhenEstimateEOff,
                !state);

        AD.eX_any_profile_value_changed_cb();
}

void
eSFADUseThisRange_toggled_cb(
        GtkToggleButton *b,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        auto state = gtk_toggle_button_get_active( b);
        gtk_widget_set_sensitive(
                (GtkWidget*)AD.eSFADHistRangeMin,
                state);
        gtk_widget_set_sensitive(
                (GtkWidget*)AD.eSFADHistRangeMax,
                state);

        AD.eX_any_profile_value_changed_cb();
}

void
bSFADApply_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;
        auto& SF = AD._p;

        gtk_widget_hide( (GtkWidget*)AD.wSFAD);

        for ( auto& H : AD.channels_visible_backup )
                H.first->hidden = H.second;
        AD.channels_visible_backup.clear();
        AD.artifacts_backup.clear_all();

        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
}

void
bSFADDismiss_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;
        auto& SF = AD._p;
        SF.artifacts_dialog_shown = false;

        gtk_widget_hide( (GtkWidget*)AD.wSFAD);

        if ( gtk_toggle_button_get_active(AD.bSFADPreview) ) {
                AD.using_channel->artifacts = AD.artifacts_backup;
                AD.using_channel->get_signal_filtered();

                gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
                gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
        }

        for ( auto& H : AD.channels_visible_backup )
                H.first->hidden = H.second;
        AD.channels_visible_backup.clear();
        AD.artifacts_backup.clear_all();
}

void
bSFADPreview_toggled_cb(
        GtkToggleButton *b,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;
        auto& SF = AD._p;

        if ( AD.suppress_preview_handler )
                return;

        if ( gtk_toggle_button_get_active(b) ) {
                SBusyBlock bb (AD.wSFAD);

                AD.orig_signal_visible_backup = AD.using_channel->draw_original_signal;
                AD.artifacts_backup = AD.using_channel->artifacts;

                AD.using_channel->detect_artifacts( (AD.W_V.down(), AD.Pp2.Pp));
                AD.using_channel->draw_original_signal = true;
                gtk_widget_set_sensitive( (GtkWidget*)AD.bSFADApply, TRUE);

                AD.channels_visible_backup.clear();
                if ( gtk_toggle_button_get_active( (GtkToggleButton*)AD.eSFADSingleChannelPreview) )
                        for ( auto& H : SF.channels ) {
                                AD.channels_visible_backup.emplace_back(
                                        &H, H.hidden);
                                if ( &H != AD.using_channel )
                                        H.hidden = true;
                        }

        } else {
                AD.using_channel->artifacts = AD.artifacts_backup;
                for ( auto& H : AD.channels_visible_backup )
                        H.first->hidden = H.second;
                AD.using_channel->draw_original_signal = AD.orig_signal_visible_backup;
                gtk_widget_set_sensitive( (GtkWidget*)AD.bSFADApply, FALSE);
        }

        SF.using_channel -> get_signal_filtered();
        AD.using_channel->calculate_dirty_percent();

        gtk_label_set_markup(
                AD.lSFADDirtyPercent,
                snprintf_buf( "%4.2f%%", AD.using_channel->percent_dirty));

        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
}

}
