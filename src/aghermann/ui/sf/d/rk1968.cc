/*
 *       File name:  aghermann/ui/sf/d/rk1968.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-01-25
 *
 *         Purpose:  scoring facility: rk1968 dialog
 *
 *         License:  GPL
 */

#include <gtk/gtk.h>
#include "rk1968.hh"

using namespace std;
using namespace agh::ui;

SScoringFacility::SRK1968Dialog&
SScoringFacility::
rk1968_d()
{
        if ( !_rk1968_d )
                _rk1968_d = new SRK1968Dialog(*this);
        return *_rk1968_d;
}


SScoringFacility::SRK1968Dialog::
SRK1968Dialog (SScoringFacility& p_)
      : SDirlevelStorableAdapter<agh::rk1968::CScoreAssistant>(
              *p_._p.ED, agh::SExpDirLevelId {p_._p.ED->group_of(p_.csubject()), p_.csubject().id, p_.session()},
              mSFRKProfiles, eSFRKProfileList, eSFRKProfileList_changed_cb_handler_id,
              bSFRKProfileSave, bSFRKProfileRevert, bSFRKProfileDiscard,
              wSFRKProfileSave, eSFRKProfileSaveName,
              eSFRKProfileSaveOriginSubject, eSFRKProfileSaveOriginExperiment, eSFRKProfileSaveOriginUser,
              bSFRKProfileSaveOK),
        suppress_preview_handler (false),
        _p (p_)
{
      // 1. widgets
        builder = gtk_builder_new();
        if ( !gtk_builder_add_from_resource( builder, "/org/gtk/aghermann/sf-rk1968.glade", NULL) )
                throw runtime_error( "Failed to load SF::rk1968 glade resource");
        gtk_builder_connect_signals( builder, NULL);

        AGH_GBGETOBJ (wSFRK);
        AGH_GBGETOBJ (bSFRKProfileRevert);
        AGH_GBGETOBJ (bSFRKProfileSave);
        AGH_GBGETOBJ (bSFRKProfileDiscard);
        AGH_GBGETOBJ (eSFRKProfileList);

        AGH_GBGETOBJ (eSFRKScript);
        AGH_GBGETOBJ (tSFRKScript);

        AGH_GBGETOBJ (lSFRKTopInfo);
        AGH_GBGETOBJ (lSFRKBottomInfo);
        AGH_GBGETOBJ (bSFRKPreview);
        AGH_GBGETOBJ (bSFRKApply);
        AGH_GBGETOBJ (bSFRKDismiss);
        AGH_GBGETOBJ (wSFRKProfileSave);
        AGH_GBGETOBJ (eSFRKProfileSaveName);
        AGH_GBGETOBJ (eSFRKProfileSaveOriginSubject);
        AGH_GBGETOBJ (eSFRKProfileSaveOriginExperiment);
        AGH_GBGETOBJ (eSFRKProfileSaveOriginUser);
        AGH_GBGETOBJ (bSFRKProfileSaveOK);

        mSFRKProfiles =
                gtk_list_store_new( 1, G_TYPE_STRING);
        gtk_combo_box_set_model_properly(
                eSFRKProfileList, mSFRKProfiles);

        // agh::ui::global::set_mono_font(
        //         (GtkWidget*)eSFRKScript, agh::ui::global::css_mono10);

        G_CONNECT_1 (wSFRK, show);
        G_CONNECT_1 (wSFRK, hide);

        set_window_size_as_screen_fraction( (GtkWindow*)wSFRK, .4, .7);

        eSFRKProfileList_changed_cb_handler_id =
                G_CONNECT_1 (eSFRKProfileList, changed);
        G_CONNECT_1 (bSFRKProfileSave,    clicked);
        G_CONNECT_1 (bSFRKProfileRevert,  clicked);
        G_CONNECT_1 (bSFRKProfileDiscard, clicked);

        g_signal_connect(
                tSFRKScript, "changed",
                (GCallback)eSFRK_any_profile_value_changed_cb,
                this);

        G_CONNECT_1 (bSFRKPreview, toggled);
        G_CONNECT_1 (bSFRKApply,   clicked);
        G_CONNECT_1 (bSFRKDismiss, clicked);

        G_CONNECT_1 (eSFRKProfileSaveName, changed);

        for ( auto& W : {eSFRKProfileSaveOriginUser, eSFRKProfileSaveOriginExperiment, eSFRKProfileSaveOriginSubject} )
                g_signal_connect(
                        W, "toggled",
                        (GCallback)eSFRK_any_profile_origin_toggled_cb,
                        this);

      // 2. dialog
        W_V.reg( tSFRKScript, &Pp2.script_contents);
        protected_up();
}

SScoringFacility::SRK1968Dialog::
~SRK1968Dialog ()
{
        gtk_widget_destroy( (GtkWidget*)wSFRK);
        g_object_unref( (GObject*)mSFRKProfiles);
        g_object_unref( (GObject*)builder);
}
