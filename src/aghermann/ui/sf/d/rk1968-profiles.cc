/*
 *       File name:  aghermann/ui/sf/d/rk1968-profiles.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-07
 *
 *         Purpose:  scoring facility rk1968 profiles (enumerating & io)
 *
 *         License:  GPL
 */

// #include "aghermann/expdesign/dirlevel.hh"
// #include "aghermann/expdesign/expdesign.hh"
// #include "aghermann/ui/misc.hh"
// #include "aghermann/ui/mw/mw.hh"
// #include "aghermann/ui/sf/channel.hh"
#include "rk1968.hh"

using namespace std;
using namespace agh::ui;
