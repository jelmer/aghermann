/*
 *       File name:  aghermann/ui/sf/d/patterns-profiles_cb.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2011-07-03
 *
 *         Purpose:  scoring facility patterns
 *
 *         License:  GPL
 */

#include <sys/time.h>

#include "aghermann/ui/misc.hh"
#include "aghermann/ui/sf/channel.hh"
#include "aghermann/ui/sf/sf.hh"
#include "aghermann/ui/mw/mw.hh"
#include "aghermann/expdesign/expdesign.hh"
#include "patterns.hh"


using namespace std;
using namespace agh::ui;


extern "C" {


void
eSFFDProfileList_changed_cb(
        GtkComboBox *combo,
        const gpointer userdata)
{
        auto& FD = *(SScoringFacility::SPatternsDialog*)userdata;

        FD.eXProfileList_changed_cb();

        if ( FD.current_profile != FD.profiles.end() ) {
                FD.thing_display_scale = FD.field_channel->signal_display_scale;
        } else
                gtk_label_set_text( FD.lSFFDParametersBrief, "");

        FD.setup_controls_for_find();

        gtk_widget_queue_draw( (GtkWidget*)FD.daSFFDThing);
}



void
bSFFDProfileSave_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& FD = *(SScoringFacility::SPatternsDialog*)userdata;

        FD.bXProfileSave_clicked_cb();

        FD.setup_controls_for_find();
}


void
eSFFDProfileSaveName_changed_cb(
        GtkEditable*,
        const gpointer userdata)
{
        auto& FD = *(SScoringFacility::SPatternsDialog*)userdata;

        FD.eXProfileSaveName_changed_cb();
}

void
eSFFD_any_profile_origin_toggled_cb(
        GtkRadioButton*,
        const gpointer userdata)
{
        auto& FD = *(SScoringFacility::SPatternsDialog*)userdata;

        FD.eX_any_profile_origin_toggled_cb();
}




void
bSFFDProfileDiscard_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& FD = *(SScoringFacility::SPatternsDialog*)userdata;

        FD.bXProfileDiscard_clicked_cb();

        FD.setup_controls_for_find();

        gtk_widget_queue_draw( (GtkWidget*)FD.daSFFDThing);
}


void
bSFFDProfileRevert_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& FD = *(SScoringFacility::SPatternsDialog*)userdata;

        FD.bXProfileRevert_clicked_cb();

        FD.setup_controls_for_find();

        FD.set_profile_manage_buttons_visibility();
}

} // extern "C"
