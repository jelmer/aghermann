/*
 *       File name:  aghermann/ui/sf/montage-menus_cb.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2011-07-02
 *
 *         Purpose:  scoring facility: montage menu callbacks
 *
 *         License:  GPL
 */

#include "aghermann/ui/misc.hh"
#include "aghermann/ui/mw/mw.hh"
#include "channel.hh"
#include "sf.hh"
#include "sf_cb.hh"
#include "d/artifacts.hh"
#include "d/filters.hh"


using namespace std;
using namespace agh::ui;

using agh::str::sasprintf;
using agh::str::homedir2tilda;


extern "C" {


void
iSFPageShowOriginal_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_original_signal = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        // prevent both being switched off
        if ( !SF.using_channel->draw_original_signal && !SF.using_channel->draw_filtered_signal )
                gtk_check_menu_item_set_active( SF.iSFPageShowProcessed,
                                                (gboolean)(SF.using_channel->draw_filtered_signal = true));
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}


void
iSFPageShowProcessed_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_filtered_signal = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        if ( !SF.using_channel->draw_filtered_signal && !SF.using_channel->draw_original_signal )
                gtk_check_menu_item_set_active( SF.iSFPageShowOriginal,
                                                (gboolean)(SF.using_channel->draw_original_signal = true));
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}


void
iSFPageUseResample_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->resample_signal = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}


void
iSFPageDrawZeroline_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_zeroline = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawEMGSteadyTone_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_emg_steady_tone = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageHide_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        SF.using_channel->hidden = true;
        // add an item to iSFPageHidden
        auto item = (GtkWidget*)(SF.using_channel->menu_item_when_hidden =
                                 (GtkMenuItem*)gtk_menu_item_new_with_label( SF.using_channel->name()));
        g_object_set(
                (GObject*)item,
                "visible", TRUE,
                NULL);
        g_signal_connect(
                (GObject*)item,
                "activate", (GCallback)iSFPageShowHidden_activate_cb,
                &SF);
        gtk_container_add(
                (GtkContainer*)SF.iiSFPageHidden,
                item);

        ++SF.n_hidden;
        gtk_widget_queue_draw(
                (GtkWidget*)SF.daSFMontage);
}


void
iSFPageShowHidden_activate_cb(
        GtkMenuItem *menuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto Ch = &SF[gtk_menu_item_get_label(menuitem)];
        Ch->hidden = false;

        SF.using_channel = Ch;
        gdk_window_get_device_position(
                gtk_widget_get_window( (GtkWidget*)SF.daSFMontage),
                global::client_pointer,
                NULL, (int*)&Ch->zeroy, NULL); //SF.find_free_space();
        SF.zeroy_before_shuffling = Ch->zeroy;
        SF.event_y_when_shuffling = (double)Ch->zeroy;
        SF.mode = SScoringFacility::TMode::shuffling_channels;

        gtk_widget_destroy( (GtkWidget*)Ch->menu_item_when_hidden);
        Ch->menu_item_when_hidden = NULL;

        --SF.n_hidden;
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageSpaceEvenly_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        SF.space_evenly();
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageLocateSelection_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.using_channel->selection_start == SF.using_channel->selection_end ) {
                SF.sb_message( "There is no selection in this channel");
        } else
                SF.set_cur_vpage(
                        SF.using_channel->selection_start_time / SF.vpagesize());
}


void
iSFPageDrawPSDProfile_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_psd = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawPSDSpectrum_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_spectrum = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawMCProfile_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_mc = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawSWUProfile_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_swu = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawEMGProfile_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_emg = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawPhasicSpindles_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_phasic_spindle = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawPhasicKComplexes_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_phasic_Kcomplex = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageDrawPhasicEyeBlinks_toggled_cb(
        GtkCheckMenuItem *checkmenuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_phasic_eyeblink = (bool)gtk_check_menu_item_get_active( checkmenuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}


void
iSFPageFilter_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& FD =  SF.filters_d();
        auto& H  = *SF.using_channel;
        FD.P = H.filters;
        FD.W_V.up();

        gtk_label_set_markup(
                FD.lSFFilterCaption,
                snprintf_buf(
                        "<big>Filters for channel <b>%s</b></big>",
                        SF.using_channel->name()));

        if ( gtk_dialog_run( FD.wSFFilters) == GTK_RESPONSE_OK ) {
                FD.W_V.down();
                H.filters = FD.P;
                H.get_signal_filtered();

                if ( H.type() == sigfile::definitions::types::eeg ) {
                        H.get_psd_course();
                        H.get_psd_in_bands();
                        H.get_spectrum( SF.cur_page());
                        H.get_mc_course();
                }
                gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);

                if ( strcmp( SF.using_channel->name(), SF._p.AghH()) == 0 )
                        SF.redraw_ssubject_timeline();
        }
}


void
iSFPageArtifactsDetect_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& AD = SF.artifacts_d();

        gtk_widget_show( (GtkWidget*)AD.wSFAD);
}


void
iSFPageArtifactsClear_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;

        char* chnamee = g_markup_escape_text( SF.using_channel->name(), -1);

        if ( SF.using_channel->artifacts().empty() ) {
                pop_ok_message( SF.wSF, "No artifacts to clear", "Channel <b>%s</b> is already clean.", chnamee);

        } else
                if ( GTK_RESPONSE_YES ==
                     pop_question(
                             SF.wSF,
                             "<b>All marked artifacts will be lost</b>",
                             "Sure to clean all artifacts in channel <b>%s</b>?",
                             chnamee) ) {

                        SF.using_channel->artifacts().clear();
                        SF.using_channel->get_signal_filtered();

                        if ( SF.using_channel->type() == sigfile::definitions::types::eeg ) {
                                SF.using_channel->get_psd_course();
                                SF.using_channel->get_psd_in_bands();
                                SF.using_channel->get_spectrum();

                                SF.redraw_ssubject_timeline();
                        }

                        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
                        gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
                }

        g_free( chnamee);
}




void
iSFPageSaveChannelAsSVG_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& ED = SF._p;
        string j_dir = ED.ED->subject_dir( SF.csubject());
        string fname = sasprintf(
                "%s/%s/%s-p%zu@%zu.svg",
                j_dir.c_str(), ED.AghD(), ED.AghT(), SF.cur_vpage(), SF.vpagesize());

        SF.using_channel->draw_for_montage( fname, SF.da_wd, SF.interchannel_gap);
        ED.sb_message(
                sasprintf(
                        "Wrote \"%s\"",
                        homedir2tilda(fname).c_str()));
}


void
iSFPageSaveMontageAsSVG_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& ED = SF._p;
        string j_dir = ED.ED->subject_dir( SF.csubject());
        string fname = sasprintf(
                        "%s/%s/montage-p%zu@%zu.svg",
                        j_dir.c_str(), ED.AghD(), SF.cur_vpage(), SF.vpagesize());

        SF.draw_montage( fname);
        ED.sb_message(
                sasprintf( "Wrote \"%s\"", homedir2tilda(fname).c_str()));
}


void
iSFPageExportSignal_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& r = SF.using_channel->crecording;
        string fname_base = r.F().filename();
        r.F().export_filtered(
                SF.using_channel->h(),
                sasprintf( "%s-filt.tsv", fname_base.c_str()));
        r.F().export_original(
                SF.using_channel->h(),
                sasprintf( "%s-filt.tsv", fname_base.c_str()));
        SF.sb_message(
                sasprintf( "Wrote \"%s-{filt,orig}.tsv\"", fname_base.c_str()));
}



void
iSFPageUseThisScale_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto sane_signal_display_scale = SF.using_channel->signal_display_scale;
        for_each( SF.channels.begin(), SF.channels.end(),
                  [&] ( SScoringFacility::SChannel& H)
                  {
                          H.signal_display_scale = sane_signal_display_scale;
                  });
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}





void
iSFPageAnnotationDelete_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.over_annotations.size() == 1 ) {
                if ( GTK_RESPONSE_YES
                     == pop_question( SF.wSF,
                                      "<b>Deleting annotation</b>",
                                      "Sure you want to delete annotation\n <b>%s</b>?",
                                      SF.over_annotations.front()->label.c_str()) )
                        SF.using_channel->annotations.remove(
                                *SF.over_annotations.front());
        } else {
                if ( GTK_RESPONSE_YES
                     == pop_question( SF.wSF,
                                      "<b>Deleting annotations</b>",
                                      "Sure you want to delete <b>%zu annotations</b>?",
                                      SF.over_annotations.size()) )
                        for ( auto &rm : SF.over_annotations )
                                SF.using_channel->annotations.remove( *rm);
        }
        SF._p.populate_mGlobalAnnotations();
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}


void
iSFPageAnnotationEdit_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        sigfile::SAnnotation *which =
                (SF.over_annotations.size() == 1)
                ? SF.over_annotations.front()
                : SF.interactively_choose_annotation();
        if ( not which )
                return;

        gtk_entry_set_text( SF.eSFAnnotationLabel, which->label.c_str());
        switch ( which->type ) {
        case sigfile::SAnnotation::TType::phasic_event_spindle:
                gtk_toggle_button_set_active( (GtkToggleButton*)SF.eSFAnnotationTypeSpindle, TRUE);
                break;
        case sigfile::SAnnotation::TType::phasic_event_K_complex:
                gtk_toggle_button_set_active( (GtkToggleButton*)SF.eSFAnnotationTypeKComplex, TRUE);
                break;
        case sigfile::SAnnotation::TType::eyeblink:
                gtk_toggle_button_set_active( (GtkToggleButton*)SF.eSFAnnotationTypeBlink, TRUE);
                break;
        case sigfile::SAnnotation::TType::plain:
        default:
                gtk_toggle_button_set_active( (GtkToggleButton*)SF.eSFAnnotationTypePlain, TRUE);
                break;
        }

        if ( GTK_RESPONSE_OK ==
             gtk_dialog_run( SF.wSFAnnotationLabel) ) {
                const char* new_label = gtk_entry_get_text( SF.eSFAnnotationLabel);
                auto new_type =
                        gtk_toggle_button_get_active( (GtkToggleButton*)SF.eSFAnnotationTypeSpindle)
                        ? sigfile::SAnnotation::TType::phasic_event_spindle
                        : gtk_toggle_button_get_active( (GtkToggleButton*)SF.eSFAnnotationTypeKComplex)
                        ? sigfile::SAnnotation::TType::phasic_event_K_complex
                        : gtk_toggle_button_get_active( (GtkToggleButton*)SF.eSFAnnotationTypeBlink)
                        ? sigfile::SAnnotation::TType::eyeblink
                        : sigfile::SAnnotation::TType::plain;

                if ( strlen(new_label) > 0 ) {
                        which->label = new_label;
                        which->type = new_type;
                        SF._p.populate_mGlobalAnnotations();
                        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
                }
        }
}


void
iSFPageAnnotationClearAll_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;

        char* chnamee = g_markup_escape_text( SF.using_channel->name(), -1);
        if ( GTK_RESPONSE_YES
             == pop_question(
                     SF.wSF,
                     "<b>Deleting annotations</b>",
                     "Sure you want to delete all annotations in channel <b>%s</b>?",
                     chnamee) ) {

                SF.using_channel->annotations.clear();

                SF._p.populate_mGlobalAnnotations();
                gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
        }
        g_free( chnamee);
}


void
iSFPageAnnotationGotoNext_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;

        if ( SF.cur_vpage() == SF.total_vpages()-1 )
                return;
        size_t p = SF.cur_vpage();
        while ( ++p < SF.total_vpages() )
                if ( SF.vpage_has_annotations( p, *SF.using_channel)) {
                        SF.sb_clear();
                        SF.set_cur_vpage( p);
                        return;
                }
        SF.sb_message( "No more annotations after this");
}

void
iSFPageAnnotationGotoPrev_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;

        if ( SF.cur_vpage() == 0 )
                return;
        size_t p = SF.cur_vpage();
        while ( --p != (size_t)-1 )
                if ( SF.vpage_has_annotations( p, *SF.using_channel)) {
                        SF.sb_clear();
                        SF.set_cur_page( p);
                        return;
                }
        SF.sb_message( "No more annotations before this");
}






void
iSFICAPageMapIC_activate_cb(
        GtkRadioMenuItem* i,
        const gpointer u)
{
        auto& SF = *(SScoringFacility*)u;
        if ( SF.suppress_redraw )
                return;
        const char *label = gtk_menu_item_get_label( (GtkMenuItem*)i);

      // find target h
        int target = -1;
        int h = 0;
        for ( auto H = SF.channels.begin(); H != SF.channels.end(); ++H, ++h )
                if ( strcmp( H->name(), label) == 0 ) {
                        target = h;
                        break;
                }
        SF.ica_map[SF.using_ic].m = target;

      // remove any previous mapping of the same target
        h = 0;
        for ( h = 0; h < (int)SF.ica_map.size(); ++h )
                if ( SF.ica_map[h].m == target && h != SF.using_ic )
                        SF.ica_map[h].m = -1;

        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}


// page selection
void
iSFPageSelectionMarkArtifact_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& H = SF.using_channel;
        SBusyBlock bb (SF.wSF);

        H->mark_region_as_artifact( true);

        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
}

void
iSFPageSelectionClearArtifact_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& H = SF.using_channel;
        SBusyBlock bb (SF.wSF);

        H->mark_region_as_artifact( false);

        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
}

void
iSFPageSelectionFindPattern_activate_cb(
        GtkMenuItem*,
        gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& H = SF.using_channel;
        H->mark_region_as_pattern();
}

void
iSFPageSelectionAnnotate_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;

        gtk_entry_set_text( SF.eSFAnnotationLabel, "");

        if ( GTK_RESPONSE_OK ==
             gtk_dialog_run( (GtkDialog*)SF.wSFAnnotationLabel) ) {
                auto new_ann = gtk_entry_get_text( SF.eSFAnnotationLabel);

                using sigfile::SAnnotation;
                auto type =
                        gtk_toggle_button_get_active( (GtkToggleButton*)SF.eSFAnnotationTypeSpindle)
                        ? SAnnotation::TType::phasic_event_spindle
                        : gtk_toggle_button_get_active( (GtkToggleButton*)SF.eSFAnnotationTypeKComplex)
                        ? SAnnotation::TType::phasic_event_K_complex
                        : gtk_toggle_button_get_active( (GtkToggleButton*)SF.eSFAnnotationTypeBlink)
                        ? SAnnotation::TType::eyeblink
                        : SAnnotation::TType::plain;

                if ( strlen( new_ann) == 0 && type == SAnnotation::TType::plain ) {
                        pop_ok_message( SF.wSF, "Give a plain annotation a name", "and try again.");
                        return;
                }

                SF.using_channel->mark_region_as_annotation( new_ann, type);

                gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
                gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);

                SF._p.populate_mGlobalAnnotations();
        }
}


void
iSFPageSelectionDrawCourse_toggled_cb(
        GtkCheckMenuItem *cb,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        SF.using_channel->draw_selection_course = gtk_check_menu_item_get_active( cb);
        if ( SF.suppress_redraw )
                return;
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageSelectionDrawEnvelope_toggled_cb(
        GtkCheckMenuItem *cb,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        SF.using_channel->draw_selection_envelope = gtk_check_menu_item_get_active( cb);
        if ( SF.suppress_redraw )
                return;
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPageSelectionDrawDzxdf_toggled_cb(
        GtkCheckMenuItem *cb,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        SF.using_channel->draw_selection_dzcdf = gtk_check_menu_item_get_active( cb);
        if ( SF.suppress_redraw )
                return;
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}



// power

void
iSFPowerExportRange_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        auto& R = SF.using_channel->crecording;

        if ( SF.using_channel->draw_psd ) {
                string fname = sasprintf(
                        "%s-psd_%g-%g.tsv",
                        R.psd_profile.fname_base().c_str(), SF.using_channel->psd.from, SF.using_channel->psd.upto);
                R.psd_profile.export_tsv(
                        SF.using_channel->psd.from, SF.using_channel->psd.upto,
                        fname);
                SF.sb_message( sasprintf( "Wrote \"%s\"", homedir2tilda(fname).c_str()));
        }
        // if ( SF.using_channel->draw_swu ) {
        //         fname_base = R.swu_profile.fname_base();
        //         snprintf_buf( "%s-swu_%g-%g.tsv",
        //                       fname_base.c_str(), SF.using_channel->swu.from, SF.using_channel->swu.upto);
        //         R.swu_profile.export_tsv(
        //                 SF.using_channel->swu.from, SF.using_channel->swu.upto,
        //                 global::buf);
        //         fname_base = global::buf; // recycle
        // }
        // if ( SF.using_channel->draw_mc ) {
        //         fname_base = R.mc_profile.fname_base();
        //         snprintf_buf( "%s-mc_%g-%g.tsv",
        //                       fname_base.c_str(),
        //                       R.freq_from + R.bandwidth*(SF.using_channel->mc.bin),
        //                       R.freq_from + R.bandwidth*(SF.using_channel->mc.bin+1));
        //         R.mc_profile.export_tsv(
        //                 SF.using_channel->mc.bin,
        //                 global::buf);
        //         fname_base = global::buf;
        // }
}

void
iSFPowerExportAll_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        const auto& SF = *(SScoringFacility*)userdata;
        auto& R = SF.using_channel->crecording;

        string fname_base;
        if ( SF.using_channel->draw_psd ) {
                string fname = sasprintf(
                        "%s-psd.tsv",
                        SF.using_channel->crecording.psd_profile.fname_base().c_str());
                R.psd_profile.export_tsv( fname);
                SF.sb_message( sasprintf( "Wrote \"%s\"", homedir2tilda(fname).c_str()));
        }
        if ( SF.using_channel->draw_swu ) {
                string fname = sasprintf(
                        "%s-swu.tsv",
                        SF.using_channel->crecording.swu_profile.fname_base().c_str());
                R.swu_profile.export_tsv( fname);
                SF.sb_message( sasprintf( "Wrote \"%s\"", homedir2tilda(fname).c_str()));
        }
        if ( SF.using_channel->draw_mc ) {
                string fname = sasprintf(
                        "%s-psd.tsv",
                        SF.using_channel->crecording.psd_profile.fname_base().c_str());
                R.psd_profile.export_tsv( fname);
                SF.sb_message( sasprintf( "Wrote \"%s\"", homedir2tilda(fname).c_str()));
        }
}

void
iSFPowerSmooth_toggled_cb(
        GtkCheckMenuItem *menuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        if ( likely (SF.using_channel->type() == sigfile::definitions::types::eeg ) ) {
                SF.using_channel->resample_power = (bool)gtk_check_menu_item_get_active( menuitem);
                SF.using_channel->get_psd_course();
                SF.using_channel->get_psd_in_bands();
                SF.using_channel->get_swu_course();
                SF.using_channel->get_mc_course();
                gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
        }
}

void
iSFPowerDrawBands_toggled_cb(
        GtkCheckMenuItem *menuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        SF.using_channel->draw_psd_bands = (bool)gtk_check_menu_item_get_active( menuitem);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
}

void
iSFPowerUseThisScale_activate_cb(
        GtkMenuItem*,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;

        auto    sane_psd_display_scale = SF.using_channel->psd.display_scale,
                sane_swu_display_scale = SF.using_channel->swu.display_scale,
                sane_mc_display_scale  = SF.using_channel->mc.display_scale;
        for ( auto& H : SF.channels ) {
                H.psd.display_scale = sane_psd_display_scale;
                H.swu.display_scale = sane_swu_display_scale;
                H.mc.display_scale  = sane_mc_display_scale;
        }
        SF.queue_redraw_all();
}

void
iSFPowerAutoscale_toggled_cb(
        GtkCheckMenuItem *menuitem,
        const gpointer userdata)
{
        auto& SF = *(SScoringFacility*)userdata;
        if ( SF.suppress_redraw )
                return;
        auto& H = *SF.using_channel;

        H.autoscale_profile = (bool)gtk_check_menu_item_get_active( menuitem);

        SF.queue_redraw_all();
}


} // extern "C"
