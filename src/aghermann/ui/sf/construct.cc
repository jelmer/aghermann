/*
 *       File name:  aghermann/ui/sf/construct.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2012-06-13
 *
 *         Purpose:  scoring facility widget construct megafun
 *
 *         License:  GPL
 */

#include <stdexcept>

#include "aghermann/ui/mw/mw.hh"
#include "aghermann/ui/ui.hh"
#include "sf.hh"
#include "sf_cb.hh"

using namespace std;
using namespace agh::ui;

SScoringFacilityWidgets::
SScoringFacilityWidgets ()
{
        builder = gtk_builder_new();
        if ( !gtk_builder_add_from_resource( builder, "/org/gtk/aghermann/sf.glade", NULL) )
                throw runtime_error( "Failed to load SF glade resource");

        gtk_builder_connect_signals( builder, NULL);
        //  we do it all mostly ourself, except for some delete-event binding to gtk_true()

        // general & montage page navigation
        AGH_GBGETOBJ (wSF);
        AGH_GBGETOBJ (lSFHint);
        AGH_GBGETOBJ (mSFScoringPageSize);
        AGH_GBGETOBJ (eSFPageSize);
        AGH_GBGETOBJ (eSFCurrentPage);
        AGH_GBGETOBJ (jSFPageNo);
        AGH_GBGETOBJ (lSFTotalPages);
        AGH_GBGETOBJ (eSFCurrentPos);
        AGH_GBGETOBJ (cSFHypnogram);
        AGH_GBGETOBJ (cSFControlBar);
        AGH_GBGETOBJ (cSFScoringModeContainer);
        AGH_GBGETOBJ (cSFICAModeContainer);

        AGH_GBGETOBJ (bSFBack);
        AGH_GBGETOBJ (bSFForward);
        AGH_GBGETOBJ (bSFGotoPrevUnscored);
        AGH_GBGETOBJ (bSFGotoNextUnscored);
        AGH_GBGETOBJ (bSFGotoPrevArtifact);
        AGH_GBGETOBJ (bSFGotoNextArtifact);
        AGH_GBGETOBJ (bSFDrawCrosshair);
        AGH_GBGETOBJ (iSFMontageMenu);
        AGH_GBGETOBJ (iSFMontageDrawOriginalSignal);
        AGH_GBGETOBJ (iSFMontageDrawProcessedSignal);
        AGH_GBGETOBJ (iSFMontageDrawZeroLine);
        AGH_GBGETOBJ (iSFMontageDrawFast);
//      AGH_GBGETOBJ (iSFMontageDraw);
        AGH_GBGETOBJ (iSFMontagePatterns);
        AGH_GBGETOBJ (iSFMontageICA);
        AGH_GBGETOBJ (iSFMontagePhaseDiff);
        AGH_GBGETOBJ (iSFMontageScoreAssist);
        AGH_GBGETOBJ (iSFMontageScoreImport);
        AGH_GBGETOBJ (iSFMontageScoreExport);
        AGH_GBGETOBJ (iSFMontageScoreClear);
        AGH_GBGETOBJ (iSFMontageClose);
        AGH_GBGETOBJ (iSFMontageCloseAndNext);

        G_CONNECT_1 (iSFMontageMenu, activate);

        G_CONNECT_2 (wSF, delete, event);
        G_CONNECT_3 (wSF, key, press, event);

        gtk_combo_box_set_model_properly(
                eSFPageSize, mSFScoringPageSize);

        G_CONNECT_1 (eSFPageSize, changed);
        G_CONNECT_2 (eSFCurrentPage, value, changed);

        G_CONNECT_1 (eSFCurrentPos, clicked);
        agh::ui::global::set_mono_font(
                (GtkWidget*)eSFCurrentPos, agh::ui::global::css_mono8);
        // auto font_desc = pango_font_description_from_string( "Mono 8");
        // gtk_widget_override_font( (GtkWidget*)eSFCurrentPos, font_desc);
        // pango_font_description_free( font_desc);

        G_CONNECT_1 (bSFForward, clicked);
        G_CONNECT_1 (bSFBack, clicked);

        G_CONNECT_1 (bSFGotoNextUnscored, clicked);
        G_CONNECT_1 (bSFGotoPrevUnscored, clicked);
        G_CONNECT_1 (bSFGotoNextArtifact, clicked);
        G_CONNECT_1 (bSFGotoPrevArtifact, clicked);

        G_CONNECT_1 (bSFDrawCrosshair, toggled);

        G_CONNECT_1 (iSFMontageDrawOriginalSignal, toggled);
        G_CONNECT_1 (iSFMontageDrawProcessedSignal, toggled);
        G_CONNECT_1 (iSFMontageDrawFast, toggled);
        G_CONNECT_1 (iSFMontageDrawZeroLine, toggled);
        G_CONNECT_1 (iSFMontagePatterns, activate);
        G_CONNECT_1 (iSFMontagePhaseDiff, activate);
        G_CONNECT_1 (iSFMontageICA, activate);
        G_CONNECT_1 (iSFMontageScoreAssist, activate);
        G_CONNECT_1 (iSFMontageScoreImport, activate);
        G_CONNECT_1 (iSFMontageScoreExport, activate);
        G_CONNECT_1 (iSFMontageScoreClear, activate);
        G_CONNECT_1 (iSFMontageClose, activate);
        G_CONNECT_1 (iSFMontageCloseAndNext, activate);

        AGH_GBGETOBJ (bSFScoreClear);
        AGH_GBGETOBJ (bSFScoreNREM1);
        AGH_GBGETOBJ (bSFScoreNREM2);
        AGH_GBGETOBJ (bSFScoreNREM3);
        AGH_GBGETOBJ (bSFScoreNREM4);
        AGH_GBGETOBJ (bSFScoreREM);
        AGH_GBGETOBJ (bSFScoreWake);
        AGH_GBGETOBJ (cSFSleepStageStats);
        AGH_GBGETOBJ (lSFPercentScored);
        AGH_GBGETOBJ (lScoreStatsNREMPercent);
        AGH_GBGETOBJ (lScoreStatsREMPercent);
        AGH_GBGETOBJ (lScoreStatsWakePercent);

        G_CONNECT_1 (bSFScoreClear, clicked);
        G_CONNECT_1 (bSFScoreNREM1, clicked);
        G_CONNECT_1 (bSFScoreNREM2, clicked);
        G_CONNECT_1 (bSFScoreNREM3, clicked);
        G_CONNECT_1 (bSFScoreNREM4, clicked);
        G_CONNECT_1 (bSFScoreREM, clicked);
        G_CONNECT_1 (bSFScoreWake, clicked);

        AGH_GBGETOBJ (daSFMontage);
        AGH_GBGETOBJ (daSFHypnogram);
        AGH_GBGETOBJ (sbSF);

        sbSFContextIdGeneral = gtk_statusbar_get_context_id( sbSF, "General context");

        G_CONNECT_1 (daSFMontage, draw);
        G_CONNECT_2 (daSFMontage, configure, event);
        G_CONNECT_3 (daSFMontage, button, press, event);
        G_CONNECT_3 (daSFMontage, button, release, event);
        G_CONNECT_2 (daSFMontage, scroll, event);
        G_CONNECT_3 (daSFMontage, motion, notify, event);
        G_CONNECT_3 (daSFMontage, leave, notify, event);

        G_CONNECT_1 (daSFHypnogram, draw);
        G_CONNECT_3 (daSFHypnogram, button, press, event);
        G_CONNECT_3 (daSFHypnogram, button, release, event);
        G_CONNECT_3 (daSFHypnogram, motion, notify, event);

        // ICA
        AGH_GBGETOBJ (eSFICARemixMode);
        AGH_GBGETOBJ (eSFICANonlinearity);
        AGH_GBGETOBJ (eSFICAApproach);
        AGH_GBGETOBJ (mSFICARemixMode);
        AGH_GBGETOBJ (mSFICANonlinearity);
        AGH_GBGETOBJ (mSFICAApproach);
        AGH_GBGETOBJ (eSFICAFineTune);
        AGH_GBGETOBJ (eSFICAStabilizationMode);
        AGH_GBGETOBJ (eSFICAa1);
        AGH_GBGETOBJ (eSFICAa2);
        AGH_GBGETOBJ (eSFICAmu);
        AGH_GBGETOBJ (eSFICAepsilon);
        AGH_GBGETOBJ (eSFICANofICs);
        AGH_GBGETOBJ (jSFICANofICs);
        AGH_GBGETOBJ (eSFICAEigVecFirst);
        AGH_GBGETOBJ (eSFICAEigVecLast);
        AGH_GBGETOBJ (jSFICAEigVecFirst);
        AGH_GBGETOBJ (jSFICAEigVecLast);
        AGH_GBGETOBJ (eSFICASampleSizePercent);
        AGH_GBGETOBJ (eSFICAMaxIterations);
        AGH_GBGETOBJ (bSFICATry);
        AGH_GBGETOBJ (bSFICAPreview);
        AGH_GBGETOBJ (bSFICAShowMatrix);
        AGH_GBGETOBJ (bSFICAApply);
        AGH_GBGETOBJ (bSFICACancel);
        AGH_GBGETOBJ (wSFICAMatrix);
        AGH_GBGETOBJ (tSFICAMatrix);

        gtk_combo_box_set_model_properly( eSFICANonlinearity, mSFICANonlinearity);
        gtk_combo_box_set_model_properly( eSFICAApproach, mSFICAApproach);
        gtk_combo_box_set_model_properly( eSFICARemixMode, mSFICARemixMode);

        auto tabarray = pango_tab_array_new( 20, FALSE);  // 20 channels is good enough
        for ( int t = 1; t < 20; ++t )
                pango_tab_array_set_tab( tabarray, t-1, PANGO_TAB_LEFT, t * 12);
        g_object_set( tSFICAMatrix,
                      "tabs", tabarray,
                      NULL);

        G_CONNECT_1 (eSFICARemixMode, changed);
        G_CONNECT_1 (eSFICANonlinearity, changed);
        G_CONNECT_1 (eSFICAApproach, changed);
        G_CONNECT_1 (eSFICAFineTune, toggled);
        G_CONNECT_1 (eSFICAStabilizationMode, toggled);
        G_CONNECT_2 (eSFICAa1, value, changed);
        G_CONNECT_2 (eSFICAa2, value, changed);
        G_CONNECT_2 (eSFICAmu, value, changed);
        G_CONNECT_2 (eSFICAepsilon, value, changed);
        G_CONNECT_2 (eSFICANofICs, value, changed);
        G_CONNECT_2 (eSFICAEigVecFirst, value, changed);
        G_CONNECT_2 (eSFICAEigVecLast, value, changed);
        G_CONNECT_2 (eSFICASampleSizePercent, value, changed);
        G_CONNECT_2 (eSFICAMaxIterations, value, changed);

        G_CONNECT_1 (bSFICATry, clicked);
        G_CONNECT_1 (bSFICAPreview, toggled);
        G_CONNECT_1 (bSFICAShowMatrix, toggled);
        G_CONNECT_1 (wSFICAMatrix, hide);

        G_CONNECT_1 (bSFICAApply, clicked);
        G_CONNECT_1 (bSFICACancel, clicked);

        // ------- menus
        AGH_GBGETOBJ (lSFOverChannel);
        AGH_GBGETOBJ (iiSFPage);
        AGH_GBGETOBJ (iiSFICAPage);
        AGH_GBGETOBJ (iiSFPageSelection);
        AGH_GBGETOBJ (iiSFPageAnnotation);
        AGH_GBGETOBJ (iiSFPageProfiles);
        AGH_GBGETOBJ (iiSFPagePhasicEvents);
        AGH_GBGETOBJ (iiSFPageHidden);
        AGH_GBGETOBJ (iiSFPower);
        AGH_GBGETOBJ (iiSFScore);
        AGH_GBGETOBJ (iSFPageShowOriginal);
        AGH_GBGETOBJ (iSFPageShowProcessed);
        AGH_GBGETOBJ (iSFPageUseResample);
        AGH_GBGETOBJ (iSFPageDrawZeroline);
        AGH_GBGETOBJ (iSFPageDrawEMGSteadyTone);
        AGH_GBGETOBJ (iSFPageProfilesSubmenuSeparator);
        AGH_GBGETOBJ (iSFPageDrawPSDProfile);
        AGH_GBGETOBJ (iSFPageDrawPSDSpectrum);
        AGH_GBGETOBJ (iSFPageDrawSWUProfile);
        AGH_GBGETOBJ (iSFPageDrawMCProfile);
        AGH_GBGETOBJ (iSFPageDrawEMGProfile);
        AGH_GBGETOBJ (iSFPageDrawPhasicSpindles);
        AGH_GBGETOBJ (iSFPageDrawPhasicKComplexes);
        AGH_GBGETOBJ (iSFPageDrawPhasicEyeBlinks);
        AGH_GBGETOBJ (iSFPageFilter);
        AGH_GBGETOBJ (iSFPageSaveChannelAsSVG);
        AGH_GBGETOBJ (iSFPageSaveMontageAsSVG);
        AGH_GBGETOBJ (iSFPageExportSignal);
        AGH_GBGETOBJ (iSFPageUseThisScale);
        AGH_GBGETOBJ (iSFPageArtifactsDetect);
        AGH_GBGETOBJ (iSFPageArtifactsClear);
        AGH_GBGETOBJ (iSFPageHide);
        AGH_GBGETOBJ (iSFPageHidden);
        AGH_GBGETOBJ (iSFPageSpaceEvenly);
        AGH_GBGETOBJ (iSFPageLocateSelection);
        AGH_GBGETOBJ (iSFPageAnnotationSeparator);
        AGH_GBGETOBJ (iSFPageAnnotationDelete);
        AGH_GBGETOBJ (iSFPageAnnotationEdit);
        AGH_GBGETOBJ (iSFPageAnnotationClearAll);
        AGH_GBGETOBJ (iSFPageAnnotationGotoNext);
        AGH_GBGETOBJ (iSFPageAnnotationGotoPrev);
        AGH_GBGETOBJ (iSFPageSelectionMarkArtifact);
        AGH_GBGETOBJ (iSFPageSelectionClearArtifact);
        AGH_GBGETOBJ (iSFPageSelectionFindPattern);
        AGH_GBGETOBJ (iSFPageSelectionAnnotate);
        AGH_GBGETOBJ (iSFPageSelectionDrawCourse);
        AGH_GBGETOBJ (iSFPageSelectionDrawEnvelope);
        AGH_GBGETOBJ (iSFPageSelectionDrawDzxdf);
        AGH_GBGETOBJ (iSFPowerExportRange);
        AGH_GBGETOBJ (iSFPowerExportAll);
        AGH_GBGETOBJ (iSFPowerSmooth);
        AGH_GBGETOBJ (iSFPowerDrawBands);
        AGH_GBGETOBJ (iSFPowerUseThisScale);
        AGH_GBGETOBJ (iSFPowerAutoscale);
        AGH_GBGETOBJ (iSFScoreAssist);
        AGH_GBGETOBJ (iSFScoreImport);
        AGH_GBGETOBJ (iSFScoreExport);
        AGH_GBGETOBJ (iSFScoreClear);

        gtk_menu_item_set_submenu( iSFPageHidden, (GtkWidget*)iiSFPageHidden);

        G_CONNECT_1 (iSFPageShowOriginal, toggled);
        G_CONNECT_1 (iSFPageShowProcessed, toggled);
        G_CONNECT_1 (iSFPageUseResample, toggled);
        G_CONNECT_1 (iSFPageDrawZeroline, toggled);
        G_CONNECT_1 (iSFPageDrawEMGSteadyTone, toggled);

        G_CONNECT_1 (iSFPageAnnotationDelete, activate);
        G_CONNECT_1 (iSFPageAnnotationEdit, activate);
        G_CONNECT_1 (iSFPageAnnotationClearAll, activate);
        G_CONNECT_1 (iSFPageAnnotationGotoPrev, activate);
        G_CONNECT_1 (iSFPageAnnotationGotoNext, activate);

        G_CONNECT_1 (iSFPageSelectionMarkArtifact, activate);
        G_CONNECT_1 (iSFPageSelectionClearArtifact, activate);
        G_CONNECT_1 (iSFPageSelectionFindPattern, activate);
        G_CONNECT_1 (iSFPageSelectionAnnotate, activate);
        G_CONNECT_1 (iSFPageSelectionDrawCourse, toggled);
        G_CONNECT_1 (iSFPageSelectionDrawEnvelope, toggled);
        G_CONNECT_1 (iSFPageSelectionDrawDzxdf, toggled);

        G_CONNECT_1 (iSFPageFilter, activate);
        G_CONNECT_1 (iSFPageSaveChannelAsSVG, activate);
        G_CONNECT_1 (iSFPageSaveMontageAsSVG, activate);
        G_CONNECT_1 (iSFPageExportSignal, activate);
        G_CONNECT_1 (iSFPageUseThisScale, activate);
        G_CONNECT_1 (iSFPageArtifactsDetect, activate);
        G_CONNECT_1 (iSFPageArtifactsClear, activate);
        G_CONNECT_1 (iSFPageHide, activate);

        G_CONNECT_1 (iSFPageSpaceEvenly, activate);
        G_CONNECT_1 (iSFPageLocateSelection, activate);

        G_CONNECT_1 (iSFPageDrawPSDProfile, toggled);
        G_CONNECT_1 (iSFPageDrawPSDSpectrum, toggled);
        G_CONNECT_1 (iSFPageDrawMCProfile, toggled);
        G_CONNECT_1 (iSFPageDrawSWUProfile, toggled);
        G_CONNECT_1 (iSFPageDrawEMGProfile, toggled);

        G_CONNECT_1 (iSFPageDrawPhasicSpindles, toggled);
        G_CONNECT_1 (iSFPageDrawPhasicKComplexes, toggled);
        G_CONNECT_1 (iSFPageDrawPhasicEyeBlinks, toggled);

        G_CONNECT_1 (iSFPowerExportRange, activate);
        G_CONNECT_1 (iSFPowerExportAll, activate);
        G_CONNECT_1 (iSFPowerSmooth, toggled);
        G_CONNECT_1 (iSFPowerDrawBands, toggled);
        G_CONNECT_1 (iSFPowerUseThisScale, activate);
        G_CONNECT_1 (iSFPowerAutoscale, toggled);

        G_CONNECT_1 (iSFScoreAssist, activate);
        G_CONNECT_1 (iSFScoreExport, activate);
        G_CONNECT_1 (iSFScoreImport, activate);
        G_CONNECT_1 (iSFScoreClear, activate);

      // petty dialogs
        // annotations
        AGH_GBGETOBJ (wSFAnnotationLabel);
        AGH_GBGETOBJ (eSFAnnotationLabel);
        AGH_GBGETOBJ (eSFAnnotationTypePlain);
        AGH_GBGETOBJ (eSFAnnotationTypeSpindle);
        AGH_GBGETOBJ (eSFAnnotationTypeKComplex);
        AGH_GBGETOBJ (eSFAnnotationTypeBlink);
        AGH_GBGETOBJ (wSFAnnotationSelector);
        AGH_GBGETOBJ (eSFAnnotationSelectorWhich);

        mSFAnnotationsAtCursor = gtk_list_store_new(1, G_TYPE_STRING);
        gtk_combo_box_set_model_properly( eSFAnnotationSelectorWhich, mSFAnnotationsAtCursor);
}




SScoringFacilityWidgets::
~SScoringFacilityWidgets ()
{
        // destroy toplevels
        gtk_widget_destroy( (GtkWidget*)wSF);
        g_object_unref( (GObject*)builder);
}
