/*
 *       File name:  aghermann/ui/globals.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-08-10
 *
 *         Purpose:  global UI-related variables
 *
 *         License:  GPL
 */

#include <gtk/gtk.h>
#include "project_strings.h"
#include "globals.hh"

using namespace std;
using namespace agh::ui::global;

char    agh::ui::global::buf[AGH_BUF_SIZE];

GdkDevice
        *agh::ui::global::client_pointer;

GtkWindow
        *agh::ui::global::main_window;


// global css

GtkCssProvider
        *agh::ui::global::css_mono8,
        *agh::ui::global::css_mono10;


// own init

int
agh::ui::global::
prepare_for_expdesign()
{
      // tell me what they are
        client_pointer =
                gdk_seat_get_pointer(
                        gdk_display_get_default_seat( gdk_display_get_default()));

        GResource
                *gresource
                = g_resource_load(
                        PACKAGE_DATADIR "/" PACKAGE "/" AGH_UI_GRESOURCE_FILE,
                        NULL);
        if ( !gresource ) {
                fprintf( stderr, "Bad or missing " PACKAGE_DATADIR "/" PACKAGE "/" AGH_UI_GRESOURCE_FILE);
                return -1;
        }
        g_resources_register( gresource);

        css_mono8 = gtk_css_provider_new();
        gtk_css_provider_load_from_data(
                css_mono8, "* {font: Mono 8}", -1, NULL);
        css_mono10 = gtk_css_provider_new();
        gtk_css_provider_load_from_data(
                css_mono8, "* {font: Mono 10}", -1, NULL);

        return 0;
}
