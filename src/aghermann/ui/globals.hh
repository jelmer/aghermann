/*
 *       File name:  aghermann/ui/globals.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2012-09-22
 *
 *         Purpose:  ui globals
 *
 *         License:  GPL
 */


#ifndef AGH_AGHERMANN_UI_GLOBALS_H_
#define AGH_AGHERMANN_UI_GLOBALS_H_

#include <gtk/gtk.h>

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;


namespace agh {
namespace ui {
namespace global {

// convenience assign-once vars
extern GtkWindow
        *main_window;

extern GdkDevice
        *client_pointer;

extern double
        hdpmm,
        vdpmm;


extern GtkCssProvider
        *css_mono8, *css_mono10;

inline void
set_mono_font(GtkWidget* w, GtkCssProvider* css)
{
        gtk_style_context_add_provider(
                gtk_widget_get_style_context( w),
                (GtkStyleProvider*)css,
                GTK_STYLE_PROVIDER_PRIORITY_USER);
}


// quick tmp storage
#define AGH_BUF_SIZE (1024*5)
extern char
        buf[AGH_BUF_SIZE];


int prepare_for_expdesign();

}
}
} // namespace agh::ui::global

#endif
