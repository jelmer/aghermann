/*
 *       File name:  aghermann/ui/forward-decls.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2010-09-03
 *
 *         Purpose:  forward declarations of ui classes
 *
 *         License:  GPL
 */


#ifndef _AGHUI_FORWARD_DECLS_H
#define _AGHUI_FORWARD_DECLS_H

namespace agh {
namespace ui {

class SSessionChooser;
class SExpDesignUI;
class SScoringFacility;
class SModelrunFacility;

}
}

#endif
