/*
 *       File name:  aghermann/ui/mf/construct.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2011-07-11
 *
 *         Purpose:  modelrun facility construct_widgets
 *
 *         License:  GPL
 */

#include "mf.hh"
#include "mf_cb.hh"

using namespace std;
using namespace agh::ui;

int
SModelrunFacility::
construct_widgets()
{
        AGH_GBGETOBJ (wModelrunFacility);
        AGH_GBGETOBJ (daMFProfile);
        AGH_GBGETOBJ (lMFLog);
        AGH_GBGETOBJ (eMFLiveUpdate);
        AGH_GBGETOBJ (eMFHighlightWake);
        AGH_GBGETOBJ (eMFHighlightNREM);
        AGH_GBGETOBJ (eMFHighlightREM);
        AGH_GBGETOBJ (eMFLiveUpdate);
        AGH_GBGETOBJ (eMFSmooth);
        AGH_GBGETOBJ (cMFControls);
        AGH_GBGETOBJ (lMFCostFunction);
        AGH_GBGETOBJ (eMFClassicFit);
        AGH_GBGETOBJ (lMFClassicFit);
        AGH_GBGETOBJ (bMFRun);
        AGH_GBGETOBJ (bMFReset);
        AGH_GBGETOBJ (bMFAccept);
        AGH_GBGETOBJ (eMFDB1);
        AGH_GBGETOBJ (eMFDB2);
        AGH_GBGETOBJ (eMFAZ1);
        AGH_GBGETOBJ (eMFAZ2);

        G_CONNECT_2 (wModelrunFacility, delete, event);
        G_CONNECT_2 (eMFSmooth, value, changed);

        G_CONNECT_1 (eMFHighlightNREM, toggled);
        G_CONNECT_1 (eMFHighlightREM, toggled);
        G_CONNECT_1 (eMFHighlightWake, toggled);
        G_CONNECT_1 (eMFClassicFit, toggled);

        G_CONNECT_1 (eMFDB1, toggled);
        G_CONNECT_1 (eMFDB2, toggled);
        G_CONNECT_1 (eMFAZ1, toggled);
        G_CONNECT_1 (eMFAZ2, toggled);

        for ( auto& T : forward_list<pair<const char*, agh::ach::TTunable>>
                { {"eMFVrs",  agh::ach::TTunable::rs },
                  {"eMFVrc",  agh::ach::TTunable::rc },
                  {"eMFVfcR", agh::ach::TTunable::fcR},
                  {"eMFVfcW", agh::ach::TTunable::fcW},
                  {"eMFVS0",  agh::ach::TTunable::S0 },
                  {"eMFVSU",  agh::ach::TTunable::SU },
                  {"eMFVta",  agh::ach::TTunable::ta },
                  {"eMFVtp",  agh::ach::TTunable::tp },
                  {"eMFVgc1", agh::ach::TTunable::gc1},
                  {"eMFVgc2", agh::ach::TTunable::gc2},
                  {"eMFVgc3", agh::ach::TTunable::gc3},
                  {"eMFVgc4", agh::ach::TTunable::gc4}} ) {
                auto W = (GtkSpinButton*)gtk_builder_get_object( builder, T.first);
                if ( W ) {
                        eMFVx[W] = T.second;
                        g_signal_connect(
                                W, "value-changed",
                                (GCallback)eMFVx_value_changed_cb,
                                this);
                } else
                        throw runtime_error (string("Missing widget: ") + T.first);
        }

        if ( not csimulation.ctl_params.AZAmendment1 ) // disable gcx unless AZAmendment is in effect
                for ( auto &T : eMFVx )
                        if ( T.second > agh::ach::TTunable::gc )
                                gtk_widget_set_sensitive( (GtkWidget*)T.first, FALSE);

        g_object_set(
                (GObject*)lMFLog,
                "tabs", pango_tab_array_new_with_positions(
                        6, TRUE,
                        PANGO_TAB_LEFT, 50,
                        PANGO_TAB_LEFT, 150,
                        PANGO_TAB_LEFT, 240,
                        PANGO_TAB_LEFT, 330,
                        PANGO_TAB_LEFT, 420,
                        PANGO_TAB_LEFT, 510),
                NULL);


        agh::ui::global::set_mono_font(
                (GtkWidget*)lMFLog, agh::ui::global::css_mono8);

        log_text_buffer = gtk_text_view_get_buffer( lMFLog);

        G_CONNECT_2 (daMFProfile, configure, event);
        G_CONNECT_1 (daMFProfile, draw);
        G_CONNECT_3 (daMFProfile, button, press, event);
        G_CONNECT_2 (daMFProfile, scroll, event);

        G_CONNECT_1 (bMFRun, clicked);
        G_CONNECT_1 (bMFReset, clicked);
        G_CONNECT_1 (bMFAccept, clicked);

        return 0;
}
