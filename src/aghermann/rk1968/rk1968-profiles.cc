/*
 *       File name:  aghermann/rk1968/rk1968-profiles.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-09
 *
 *         Purpose:  scoring assistant, profiles handling
 *
 *         License:  GPL
 */


#include <sys/stat.h>

#include "common/fs.hh"
#include "common/config-validate.hh"
#include "aghermann/expdesign/expdesign.hh"

#include "rk1968.hh"


using namespace std;
using namespace agh::rk1968;
