/*
 *       File name:  libsigfile/channel.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2011-11-10
 *
 *         Purpose:  representation of a biosig channel
 *
 *         License:  GPL
 */

#ifndef AGH_SIGFILE_CHANNEL_H_
#define AGH_SIGFILE_CHANNEL_H_

#include <cstring>
#include <tuple>
#include <string>
#include <sstream>

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif


using namespace std;

namespace sigfile {

// we want scoped enums with basic arith support, so:
namespace definitions {
enum class types {
        invalid,
        edf_annotation,
        eeg, eog, emg, ecg, erg,
        nc, meg, mcg, ep, temp, resp, sao2, light, sound, event, freq,
};

using TNameWithType = tuple<const char* const, types>;

} // namespace SignalTypes


struct SChannel {

        static const char*
        type_s(definitions::types);

        static const definitions::TNameWithType&
        classify_channel( const string&);

        static tuple<definitions::types, const char* const, string>
        figure_type_and_name( const string&);

        static bool is_fftable( definitions::types t)
        { return t == definitions::types::eeg; }

      // ctor
        SChannel (const string& h)
        {
                tie (_type, _canonical_name, _custom_name) = figure_type_and_name(h);
        }
        SChannel (const SChannel& rv) :
                _type (rv._type),
                _canonical_name (rv._canonical_name),
                _custom_name (rv._custom_name)
        {}
        SChannel () = default;

        definitions::types
        type() const
        { return _type; }

        const char*
        type_s() const
        { return type_s(_type); }

        const char*
        canonical_name() const
        { return _canonical_name; }

        const char*
        custom_name() const
        { return _custom_name.c_str(); }

        bool
        is_fftable() const
        { return is_fftable( _type); }

    private:
        definitions::types
                _type;
        const char*
                _canonical_name;  // points to one in NAMES
        string  _custom_name;     // as given

    public:
        // compares by channel actual locations, antero-posteriorly
        bool operator<( const SChannel& rv) const;

        bool operator==( const SChannel& rv) const
        {
                return  _canonical_name == rv._canonical_name &&  // yes we are comparing pointers
                        _custom_name == rv._custom_name;
        }
        bool fuzzy_equal( const string& rv) const
        {
                return *this == SChannel (rv);
        }
};

template <typename C>
string
join_channel_names( const C& l, const char* sep)
{
        if ( l.empty() )
                return "";
        ostringstream recv;
        auto I = l.begin();
        for ( ; next(I) != l.end(); ++I )
                recv << I->custom_name() << sep;
        recv << I->custom_name();
        return recv.str();
}


} // namespace sigfile

#endif
