/*
 *       File name:  libsigfile/channel.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2011-11-10
 *
 *         Purpose:  a string-based class representing a biosig channel
 *
 *         License:  GPL
 */


#include <map>
#include <vector>
#include <algorithm>
#include "common/string.hh"
#include "channel.hh"

using namespace std;

using namespace sigfile;
using sigfile::SChannel;

namespace {
using definitions::types;

const map<types, const char*> TYPES = {
	{types::edf_annotation, "EDF Annotations"},
        {types::eeg,     "EEG"},
        {types::eog,     "EOG"},
        {types::emg,     "EMG"},
        {types::ecg,     "ECG"},
        {types::erg,     "ERG"},
        {types::nc,      "NC" },
        {types::meg,     "MEG"},
        {types::mcg,     "MCG"},
        {types::ep,      "EP" },
        {types::temp,    "Temp"},
        {types::resp,    "Resp"},
        {types::sao2,    "SaO2"},
        {types::light,   "Light"},
        {types::sound,   "Sound"},
        {types::event,   "Event"},
        {types::freq,    "Freq"},
        {types::invalid, "(invalid type)"},
};

#define Q(A, B) {make_tuple(A, types::B)}
const vector<definitions::TNameWithType> NAMES = {
        Q("Nz",  eeg),
        Q("Fp1", eeg), Q("Fpz", eeg), Q("Fp2", eeg),
        Q("AF7", eeg), Q("AF3", eeg), Q("AFz", eeg), Q("AF4", eeg), Q("AF8", eeg),
        Q("F9",  eeg), Q("F7",  eeg), Q("F5",  eeg), Q("F3",  eeg), Q("F1",  eeg), Q("Fz",  eeg), Q("F2",  eeg), Q("F4",  eeg), Q("F6",  eeg), Q("F8",   eeg), Q("F10",  eeg),
        Q("FT9", eeg), Q("FT7", eeg), Q("FC5", eeg), Q("FC3", eeg), Q("FC1", eeg), Q("FCz", eeg), Q("FC2", eeg), Q("FC4", eeg), Q("FC6", eeg), Q("FCT8", eeg), Q("FT10", eeg),
        Q("A1",  eeg), Q("T9",  eeg), Q("T7",  eeg), Q("C5",  eeg), Q("C3",  eeg), Q("C1",  eeg), Q("Cz",  eeg), Q("C2",  eeg), Q("C4",  eeg), Q("C6",   eeg), Q("T8",   eeg), Q("T10", eeg), Q("A2", eeg),
        Q("TP9", eeg), Q("TP7", eeg), Q("CP5", eeg), Q("CP3", eeg), Q("CP1", eeg), Q("CPz", eeg), Q("CP2", eeg), Q("CP4", eeg), Q("CP6", eeg), Q("TP8",  eeg), Q("TP10", eeg),
        Q("P9",  eeg), Q("P7",  eeg), Q("P5",  eeg), Q("P3",  eeg), Q("P1",  eeg), Q("Pz",  eeg), Q("P2",  eeg), Q("P4",  eeg), Q("P6",  eeg), Q("P8",   eeg), Q("P10",  eeg),
        Q("PO7", eeg), Q("PO3", eeg), Q("POz", eeg), Q("PO4", eeg), Q("PO8", eeg),
        Q("O1",  eeg), Q("Oz",  eeg), Q("O2",  eeg),
        Q("Iz",  eeg),

        Q("Left", eog), Q("Right", eog),

        Q("Chin", emg),
};

const sigfile::definitions::TNameWithType invalid_name = Q("(invalid name)", invalid);
#undef Q

} // namespace anonymous


const char*
SChannel::type_s(definitions::types t)
{
        return TYPES.at(t);
}



const definitions::TNameWithType&
SChannel::
classify_channel( const string& x)
{
        auto tI = find_if(NAMES.begin(), NAMES.end(),
                          [&x](const definitions::TNameWithType& y)
                          {
                                  auto& canonical = get<0>(y);
                                  return 0 == strncasecmp(x.c_str(), canonical, strlen(canonical));
                          });
        return ( tI == NAMES.end() ) ? invalid_name : *tI;
}



tuple<definitions::types, const char* const, string>
SChannel::
figure_type_and_name( const string& h_)
{
        const auto TT = agh::str::tokens( h_, " ");
        auto& T0 = *TT.begin();
        const char* canonical_name;
        definitions::types derived_type;

        if ( TT.size() > 2 )
                ; // APPLOG_WARN ("Discarding all but the first two tokens to define channel type and name: %s", h_.c_str());

        if ( TT.size() >= 2 ) {
                auto& T1 = *next(TT.begin());
                tie(canonical_name, derived_type) = classify_channel(T1);
                if ( derived_type == definitions::types::invalid ) {
                        // APPLOG_ERROR ("Unknown signal type \"%s\"", TT[0].c_str());
                        // no central log facility for edfhed
                        return make_tuple(derived_type, get<0>(invalid_name), T1);
                }
                // the referenced channel name only need to match the
                // supplied name at the beginning, ignoring case;
                // whatever it is, is returned verbatim (to be stored
                // in _custom_name field)
                auto derived_type_s = type_s(derived_type);
                if ( 0 != strncasecmp(T0.c_str(), derived_type_s, strlen(derived_type_s)) ) {
                        // APPLOG_ERROR ("Indicated signal type \"%s\" does not match inferred type of channel \"%s\"", TT[1].c_str(), _SignalTypes.at(t));
                        return make_tuple(derived_type, canonical_name, T1);
                }
                return make_tuple(derived_type, canonical_name, T1);

        } else if ( TT.size() == 1 ) {
                tie(canonical_name, derived_type) = classify_channel(T0);
                return make_tuple(derived_type, canonical_name, T0);
        } else
                return make_tuple(definitions::types::invalid, get<0>(invalid_name), "");
}

bool
SChannel::
operator<( const SChannel& rv) const
{
        return  find(NAMES.begin(), NAMES.end(), make_tuple(   _canonical_name,    _type)) <
                find(NAMES.begin(), NAMES.end(), make_tuple(rv._canonical_name, rv._type));
}
