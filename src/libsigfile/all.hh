/*
 *       File name:  libsigfile/all.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *
 * Initial version:  2013-07-07
 *
 *         Purpose:  include headers for all EEG source types
 *
 *         License:  GPL
 */

#ifndef AGH_LIBSIGFILE_ALL_H_
#define AGH_LIBSIGFILE_ALL_H_

#include "edf.hh"
#include "tsv.hh"

#endif
