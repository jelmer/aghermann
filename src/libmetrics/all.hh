/*
 *       File name:  libmetrics/all.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 *
 * Initial version:  2013-05-23
 *
 *         Purpose:  
 *
 *         License:  GPL
 */

#ifndef AGH_LIBMETRICS_ALL_H_
#define AGH_LIBMETRICS_ALL_H_

#include "psd.hh"
#include "mc.hh"
#include "swu.hh"

#endif
