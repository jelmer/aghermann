/*
 *       File name:  libsigproc/forward-decls.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-07
 *
 *         Purpose:  
 *
 *         License:  GPL
 */

#ifndef AGH_LIBSIGPROC_FORWARD_DECLS_H_
#define AGH_LIBSIGPROC_FORWARD_DECLS_H_

namespace sigproc {

template <typename T> struct SSignalRef;

template <typename T> struct SCachedEnvelope;
template <typename T> struct SCachedDzcdf;
template <typename T> struct SCachedLowPassCourse;
template <typename T> struct SCachedBandPassCourse;

} // namespace sigproc

#endif
